#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    prevXgammaPosi=50;  
    SysGammaR=SysGammaG=SysGammaB=50; 

    rgbgammaconf = new QSettings("xgammagui.conf",QSettings::IniFormat); 

    SysGammaR=rgbgammaconf->value("rgamma").toInt();
    SysGammaG=rgbgammaconf->value("ggamma").toInt();
    SysGammaB=rgbgammaconf->value("bgamma").toInt();

    if(SysGammaR==SysGammaB&&SysGammaR==SysGammaG&&SysGammaB==SysGammaG)
    {
        ui->hsbXgamma->setValue(5);
        ui->hsbXgamma->setValue(SysGammaR);
    }
    else
    {
        ui->hsbXgammaR->setValue(SysGammaR);
        ui->hsbXgammaG->setValue(SysGammaG);
        ui->hsbXgammaB->setValue(SysGammaB);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_hsbXgamma_valueChanged(int value)
{
    ui->hsbXgammaR->setValue(ui->hsbXgammaR->value()-(prevXgammaPosi-value));
    ui->hsbXgammaB->setValue(ui->hsbXgammaB->value()-(prevXgammaPosi-value));
    ui->hsbXgammaG->setValue(ui->hsbXgammaG->value()-(prevXgammaPosi-value));

    ui->lblXgamma->setText(QString::number(value*0.02));
    ui->lblXgammaR->setText(QString::number(ui->hsbXgammaR->value()*0.02));
    ui->lblXgammaG->setText(QString::number(ui->hsbXgammaG->value()*0.02));
    ui->lblXgammaB->setText(QString::number(ui->hsbXgammaB->value()*0.02));

    prevXgammaPosi=value;
}

void MainWindow::on_hsbXgammaR_valueChanged(int value)
{
    ui->lblXgammaR->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -rgamma "+QString::number(value*0.02));

    rgbgammaconf->setValue("rgamma",QString::number(value));
}

void MainWindow::on_hsbXgammaB_valueChanged(int value)
{
    ui->lblXgammaB->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -bgamma "+QString::number(value*0.02));

    rgbgammaconf->setValue("bgamma",QString::number(value));
}

void MainWindow::on_hsbXgammaG_valueChanged(int value)
{
    ui->lblXgammaG->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -ggamma "+QString::number(value*0.02));

    rgbgammaconf->setValue("ggamma",QString::number(value));
}

void MainWindow::on_pbRestor_clicked()
{
    ui->hsbXgamma->setValue(5);
    ui->hsbXgamma->setValue(50);
    string path(getenv("HOME"));
    path += "/.config/autostart/xgammaset.desktop";
    string syscmd="rm -rf ";
    syscmd += path;
    system(syscmd.c_str());
    //QMessageBox::information(NULL,"Systemc Mesegi","Antac systemc Gamma hi zenkon restorre!");
}

void MainWindow::on_pbSave_clicked()
{
    ofstream file1;
    string path(getenv("HOME"));
    path += "/.config/autostart/xgammaset.desktop";
    file1.open(path.c_str());
    file1<<"[Desktop Entry]\nType=Application\nEncoding=UTF-8\nVersion=1.0\nName=XGammaSet\nComment=XGammaSet\nExec=sh -c \"sleep 10 && xgamma -rgamma "+QString::number(ui->hsbXgammaR->value()*0.02).toStdString()+" -ggamma "+QString::number(ui->hsbXgammaG->value()*0.02).toStdString()+" -bgamma "+QString::number(ui->hsbXgammaB->value()*0.02).toStdString()+"\"\nX-GNOME-Autostart-enabled=true";
    file1.close();
    string syscmd="chmod +x ";
    syscmd += path;
    system(syscmd.c_str());
}
