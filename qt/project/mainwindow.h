#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <ostream>
#include <fstream>

#include <QMainWindow>
#include <QProcess>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_hsbXgamma_valueChanged(int value);

    void on_hsbXgammaR_valueChanged(int value);

    void on_hsbXgammaB_valueChanged(int value);

    void on_hsbXgammaG_valueChanged(int value);

    void on_pbRestore_clicked();

    void on_pbSave_clicked();

private:
    Ui::MainWindow *ui;
    int prevXgammaPosi;
    int settingsR;
    int settingsG;
    int settingsB;
    QSettings *settings;
};
#endif // MAINWINDOW_H
