#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    prevXgammaPosi=50;
    settingsR=settingsG=settingsB=50;
    settings = new QSettings("xgammagui.conf",QSettings::IniFormat);
    settingsR=settings->value("rgamma").toInt();
    settingsG=settings->value("ggamma").toInt();
    settingsB=settings->value("bgamma").toInt();
    if(settingsR==settingsG && settingsG==settingsB)
    {
        ui->hsbXgamma->setValue(5);
        ui->hsbXgamma->setValue(settingsR);
    }
    else
    {
        ui->hsbXgammaR->setValue(settingsR);
        ui->hsbXgammaG->setValue(settingsG);
        ui->hsbXgammaB->setValue(settingsB);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_hsbXgamma_valueChanged(int value)
{
    ui->hsbXgammaR->setValue(ui->hsbXgammaR->value()-(prevXgammaPosi-value));
    ui->hsbXgammaB->setValue(ui->hsbXgammaB->value()-(prevXgammaPosi-value));
    ui->hsbXgammaG->setValue(ui->hsbXgammaG->value()-(prevXgammaPosi-value));

    ui->lblXgamma->setText(QString::number(value*0.02));
    ui->lblXgammaR->setText(QString::number(ui->hsbXgammaR->value()*0.02));
    ui->lblXgammaG->setText(QString::number(ui->hsbXgammaG->value()*0.02));
    ui->lblXgammaB->setText(QString::number(ui->hsbXgammaB->value()*0.02));

    prevXgammaPosi=value;
}

void MainWindow::on_hsbXgammaR_valueChanged(int value)
{
    ui->lblXgammaR->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -rgamma "+QString::number(value*0.02));

    settings->setValue("rgamma",QString::number(value));
}

void MainWindow::on_hsbXgammaB_valueChanged(int value)
{
    ui->lblXgammaB->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -bgamma "+QString::number(value*0.02));

    settings->setValue("bgamma",QString::number(value));
}

void MainWindow::on_hsbXgammaG_valueChanged(int value)
{
    ui->lblXgammaG->setText(QString::number(value*0.02));

    QProcess::execute("xgamma -ggamma "+QString::number(value*0.02));

    settings->setValue("ggamma",QString::number(value));
}


void MainWindow::on_pbRestore_clicked()
{
    ui->hsbXgamma->setValue(5);
    ui->hsbXgamma->setValue(50);
    std::string path(getenv("HOME"));
    path += "/.config/autostart/xgammaset.desktop";
    std::string syscmd="rm -rf ";
    syscmd += path;
    system(syscmd.c_str());
}

void MainWindow::on_pbSave_clicked()
{
    std::ofstream file1;
    std::string path(std::getenv("HOME"));
    path += "/.config/autostart/xgammaset.desktop";
    file1.open(path.c_str());
    file1<<"[Desktop Entry]\nType=Application\nEncoding=UTF-8\nVersion=1.0\nName=XGammaSet\nComment=XGammaSet\nExec=sh -c \"sleep 10 && xgamma -rgamma "+QString::number(ui->hsbXgammaR->value()*0.02).toStdString()+" -ggamma "+QString::number(ui->hsbXgammaG->value()*0.02).toStdString()+" -bgamma "+QString::number(ui->hsbXgammaB->value()*0.02).toStdString()+"\"\nX-GNOME-Autostart-enabled=true";
    file1.close();
    std::string syscmd="chmod +x ";
    syscmd += path;
    system(syscmd.c_str());
}
