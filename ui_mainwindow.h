/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *lblTitle;
    QLabel *label;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_5;
    QLabel *phoPeople;
    QLabel *phoCity;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *lblXgammaT;
    QSlider *hsbXgamma;
    QLabel *lblXgamma;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lblXgammaRT;
    QSlider *hsbXgammaR;
    QLabel *lblXgammaR;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lblXgammaGT;
    QSlider *hsbXgammaG;
    QLabel *lblXgammaG;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lblXgammaBT;
    QSlider *hsbXgammaB;
    QLabel *lblXgammaB;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pbSave;
    QPushButton *pbRestor;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(592, 573);
        MainWindow->setMinimumSize(QSize(400, 410));
        MainWindow->setMaximumSize(QSize(592, 573));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/Computer alt 2.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        lblTitle = new QLabel(centralWidget);
        lblTitle->setObjectName(QString::fromUtf8("lblTitle"));
        lblTitle->setGeometry(QRect(10, 10, 261, 31));
        lblTitle->setScaledContents(false);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 50, 361, 31));
        label->setWordWrap(true);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 90, 565, 222));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        phoPeople = new QLabel(layoutWidget);
        phoPeople->setObjectName(QString::fromUtf8("phoPeople"));
        phoPeople->setPixmap(QPixmap(QString::fromUtf8(":/images/gamma.jpg")));
        phoPeople->setScaledContents(true);

        horizontalLayout_5->addWidget(phoPeople);

        phoCity = new QLabel(layoutWidget);
        phoCity->setObjectName(QString::fromUtf8("phoCity"));
        phoCity->setPixmap(QPixmap(QString::fromUtf8(":/images/city1.jpg")));
        phoCity->setScaledContents(true);

        horizontalLayout_5->addWidget(phoCity);

        layoutWidget1 = new QWidget(centralWidget);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 330, 561, 144));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lblXgammaT = new QLabel(layoutWidget1);
        lblXgammaT->setObjectName(QString::fromUtf8("lblXgammaT"));
        lblXgammaT->setMinimumSize(QSize(60, 0));

        horizontalLayout->addWidget(lblXgammaT);

        hsbXgamma = new QSlider(layoutWidget1);
        hsbXgamma->setObjectName(QString::fromUtf8("hsbXgamma"));
        hsbXgamma->setMinimum(5);
        hsbXgamma->setMaximum(100);
        hsbXgamma->setValue(50);
        hsbXgamma->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(hsbXgamma);

        lblXgamma = new QLabel(layoutWidget1);
        lblXgamma->setObjectName(QString::fromUtf8("lblXgamma"));
        lblXgamma->setMinimumSize(QSize(30, 0));

        horizontalLayout->addWidget(lblXgamma);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lblXgammaRT = new QLabel(layoutWidget1);
        lblXgammaRT->setObjectName(QString::fromUtf8("lblXgammaRT"));
        lblXgammaRT->setMinimumSize(QSize(60, 0));

        horizontalLayout_2->addWidget(lblXgammaRT);

        hsbXgammaR = new QSlider(layoutWidget1);
        hsbXgammaR->setObjectName(QString::fromUtf8("hsbXgammaR"));
        hsbXgammaR->setMinimum(5);
        hsbXgammaR->setMaximum(100);
        hsbXgammaR->setValue(50);
        hsbXgammaR->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(hsbXgammaR);

        lblXgammaR = new QLabel(layoutWidget1);
        lblXgammaR->setObjectName(QString::fromUtf8("lblXgammaR"));
        lblXgammaR->setMinimumSize(QSize(30, 0));

        horizontalLayout_2->addWidget(lblXgammaR);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lblXgammaGT = new QLabel(layoutWidget1);
        lblXgammaGT->setObjectName(QString::fromUtf8("lblXgammaGT"));
        lblXgammaGT->setMinimumSize(QSize(60, 0));

        horizontalLayout_3->addWidget(lblXgammaGT);

        hsbXgammaG = new QSlider(layoutWidget1);
        hsbXgammaG->setObjectName(QString::fromUtf8("hsbXgammaG"));
        hsbXgammaG->setMinimum(5);
        hsbXgammaG->setMaximum(100);
        hsbXgammaG->setPageStep(10);
        hsbXgammaG->setValue(50);
        hsbXgammaG->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(hsbXgammaG);

        lblXgammaG = new QLabel(layoutWidget1);
        lblXgammaG->setObjectName(QString::fromUtf8("lblXgammaG"));
        lblXgammaG->setMinimumSize(QSize(30, 0));

        horizontalLayout_3->addWidget(lblXgammaG);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lblXgammaBT = new QLabel(layoutWidget1);
        lblXgammaBT->setObjectName(QString::fromUtf8("lblXgammaBT"));
        lblXgammaBT->setMinimumSize(QSize(60, 0));

        horizontalLayout_4->addWidget(lblXgammaBT);

        hsbXgammaB = new QSlider(layoutWidget1);
        hsbXgammaB->setObjectName(QString::fromUtf8("hsbXgammaB"));
        hsbXgammaB->setMinimum(5);
        hsbXgammaB->setMaximum(100);
        hsbXgammaB->setValue(50);
        hsbXgammaB->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(hsbXgammaB);

        lblXgammaB = new QLabel(layoutWidget1);
        lblXgammaB->setObjectName(QString::fromUtf8("lblXgammaB"));
        lblXgammaB->setMinimumSize(QSize(30, 0));

        horizontalLayout_4->addWidget(lblXgammaB);


        verticalLayout->addLayout(horizontalLayout_4);

        layoutWidget2 = new QWidget(centralWidget);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(410, 520, 178, 30));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        pbSave = new QPushButton(layoutWidget2);
        pbSave->setObjectName(QString::fromUtf8("pbSave"));

        horizontalLayout_6->addWidget(pbSave);

        pbRestor = new QPushButton(layoutWidget2);
        pbRestor->setObjectName(QString::fromUtf8("pbRestor"));

        horizontalLayout_6->addWidget(pbRestor);

        MainWindow->setCentralWidget(centralWidget);
        layoutWidget->raise();
        layoutWidget->raise();
        lblTitle->raise();
        label->raise();
        layoutWidget->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "X Gamma GUI", nullptr));
        lblTitle->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">Set color gamma</span></p></body></html>", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p>Adjust sliders and click Save pn finish.</p></body></html>", nullptr));
        phoPeople->setText(QString());
        phoCity->setText(QString());
        lblXgammaT->setText(QCoreApplication::translate("MainWindow", "Gamma", nullptr));
        lblXgamma->setText(QCoreApplication::translate("MainWindow", "1.0", nullptr));
        lblXgammaRT->setText(QCoreApplication::translate("MainWindow", "Red", nullptr));
        lblXgammaR->setText(QCoreApplication::translate("MainWindow", "1.0", nullptr));
        lblXgammaGT->setText(QCoreApplication::translate("MainWindow", "Green", nullptr));
        lblXgammaG->setText(QCoreApplication::translate("MainWindow", "1.0", nullptr));
        lblXgammaBT->setText(QCoreApplication::translate("MainWindow", "Blue", nullptr));
        lblXgammaB->setText(QCoreApplication::translate("MainWindow", "1.0", nullptr));
        pbSave->setText(QCoreApplication::translate("MainWindow", "&Save", nullptr));
        pbRestor->setText(QCoreApplication::translate("MainWindow", "&Restore", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
